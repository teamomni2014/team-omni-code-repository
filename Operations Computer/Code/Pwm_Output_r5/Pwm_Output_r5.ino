#include <PWM.h>

int oPin, iPin; //Initialize input and output pins (for loop)
int lowOut = 22; //Pin of S1
int highOut = 25; //Pin of !E
int lowIn = 2; //Pin of Y0
int highIn = 9; //Pin of Y7
int pwmpin = 11; //Pin for the frequency pin
int sensorIn = 0; //Initialize sensor variable
int temp = 0; //Variables for the while loop
int del = 500; //Variable for the delay of PulseIn


double v0, v1, v2, v3, v4, v5, v6, v7; //Frequency output values
int32_t freq = 50000; //Frequency output in Hz

void setup() {
  
  //initialize all timers except for 0, to save time keeping functions
  InitTimersSafe(); 

  //sets the frequency for the specified pin
  bool success = SetPinFrequencySafe(pwmpin, freq);
  
  //Begin serial communications
  Serial.begin(9600);
  
  //If frequency was set, print success
 if (success){
    Serial.println("Success!");
  }
  else{
    Serial.println("Failure");
  }
  
  //Configure all output pins
  for (oPin = lowOut; oPin <= highOut; oPin++){
    pinMode(oPin, OUTPUT);
    Serial.println(oPin);
  }
  //Configure all input pins
  for (iPin = lowIn; iPin <= highIn; iPin++){
    pinMode(iPin, INPUT);
    Serial.println(iPin);
  }
  //COnfigure PWM output pin
  pinMode(pwmpin, OUTPUT);
}

void loop() {
  pwmWrite(pwmpin, 131); //Set pwm pin to have a 50% duty cycle
  temp = Serial.read(); //Buffer for SensorIn variable
  if (temp == 'a'){ //Only start if temp = a. Can be easily changed to include multiple starting points
    sensorIn = temp;
  }
  muxControl(); //Run the function to control the DeMux
  //readFreq(); //Testing Purposes
  //printFreq(); //Testing Purposes
}



void muxControl(){ //Controls the DeMux
  switch(sensorIn){
    case 'a': //oscillate between Yo and Y1 (000 => 001)
    temp = 0; //Reset the temp variable
    digitalWrite(highOut, LOW);  //!E pin
    digitalWrite(highOut-1, LOW);  //S3 pin
    digitalWrite(lowOut+1, LOW);  //S2 pin
      while (sensorIn == 'a'){
        
        digitalWrite(lowOut, LOW);  //S1 pin
        delay(500); //Delay by half a second to allow the Mux to properly change channels
        readFreq();
        printFreq();
        digitalWrite(lowOut, HIGH); //S1 pin
        delay(500); //Delay by half a second to allow the Mux to properly change channels
        readFreq();
        printFreq();
        temp = Serial.read();
        
        if (temp == 'b'){ //Only allow the code to exit the loop if sensor 2 is tripped (b is inputed through serial)
          sensorIn = temp;
        }
       // Serial.println(temp); 
       // Serial.println(sensorIn); 
        }
    break;
    
    case 'b': //Oscillate between Y1 and Y2 (001 => 010)
    temp = 0;
    digitalWrite(highOut, LOW);  //!E pin
    digitalWrite(highOut-1, LOW);  //S3 pin  
    while (sensorIn == 'b'){
      digitalWrite(lowOut+1, LOW);  //S2 pin
      digitalWrite(lowOut, HIGH);  //S1 pin
      delay(500); //Delay by half a second to allow the Mux to properly change channels
      readFreq();
      printFreq();
      digitalWrite(lowOut+1, HIGH); //S2 pin
      digitalWrite(lowOut, LOW);  //S1 pin
      delay(500); //Delay by half a second to allow the Mux to properly change channels
      readFreq();
      printFreq();
      temp = Serial.read();
      if (temp == 'c'){ //Only allow the code to exit the loop if sensor 3 is tripped (c is inputed through serial)
        sensorIn = temp;
      }
      //Serial.println(temp);
      //Serial.println(sensorIn);
    }
    break;
   
    case 'c': //oscillate between Y2 and Y3 (010 => 011)
    temp = 0;
    digitalWrite(highOut, LOW);  //!E pin
    digitalWrite(highOut-1, LOW);  //S3 pin  
    while (sensorIn == 'c'){
      digitalWrite(lowOut+1, HIGH);  //S2 pin
      digitalWrite(lowOut, LOW);  //S1 pin
      delay(500); //Delay by half a second to allow the Mux to properly change channels
      readFreq();
      printFreq();
      digitalWrite(lowOut+1, HIGH); //S3 pin
      digitalWrite(lowOut, HIGH);  //S1 pin
      delay(500); //Delay by half a second to allow the Mux to properly change channels
      readFreq();
      printFreq();
      temp = Serial.read();
      if (temp == 'd'){ //Only allow the code to exit the loop if sensor 4 is tripped (d is inputed through serial)
        sensorIn = temp;
      }
      //Serial.println(sensorIn);
    }
   break;
  
  case 'd': //Oscillate between Y3 and Y4 (011 => 100)
  digitalWrite(highOut, LOW);  //!E pin
  temp = 0;
  while (sensorIn == 'd'){
    digitalWrite(highOut-1, LOW);   //S3 pin
    digitalWrite(lowOut+1, HIGH);  //S2 pin
    digitalWrite(lowOut, HIGH);  //S1 pin
    delay(500); //Delay by half a second to allow the Mux to properly change channels
    readFreq();
    printFreq();
    digitalWrite(highOut-1, HIGH); //S3 pin
    digitalWrite(lowOut+1, LOW);  //S2 pin
    digitalWrite(lowOut, LOW);  //S1 pin
    delay(500); //Delay by half a second to allow the Mux to properly change channels
    readFreq();
    printFreq();
    temp = Serial.read();
    if (temp == 'e'){ //Only allow the code to exit the loop if sensor 5 is tripped (e is inputed through serial)
        sensorIn = temp;
      }
    //Serial.println(temp);
    //Serial.println(sensorIn);
  }
  break;
  
  case 'e': //Oscillate between Y4 and Y5 (100 => 101)
  temp = 0;
  digitalWrite(highOut, LOW);  //!E pin
  digitalWrite(highOut-1, HIGH); //S3 pin
  digitalWrite(lowOut+1, LOW);  //S2 pin
  while (sensorIn == 'e'){
    digitalWrite(lowOut, LOW);  //S1 pin
    delay(500); //Delay by half a second to allow the Mux to properly change channels
    readFreq();
    printFreq();
    digitalWrite(lowOut, HIGH);  //S1 pin
    delay(500); //Delay by half a second to allow the Mux to properly change channels
    readFreq();
    printFreq();
    temp = Serial.read();
    if (temp == 'f'){ //Only allow the code to exit the loop if sensor 6 is tripped (f is inputed through serial)
        sensorIn = temp;
      }
    //Serial.println(temp);
    //Serial.println(sensorIn);
  }
  break;
  
  case 'f': //oscillate between Y5 and Y6 (101 => 110)
  temp = 0;
  digitalWrite(highOut, LOW);  //!E pin
  digitalWrite(highOut-1, HIGH);  //S3 pin  
  while (sensorIn == 'f'){
    digitalWrite(lowOut+1, LOW);  //S2 pin
    digitalWrite(lowOut, HIGH);  //S1 pin
    delay(500); //Delay by half a second to allow the Mux to properly change channels
    readFreq();
    printFreq();
    digitalWrite(lowOut+1, HIGH); //S3 pin
    digitalWrite(lowOut, LOW);  //S1 pin
    delay(500); //Delay by half a second to allow the Mux to properly change channels
    readFreq();
    printFreq();
    temp = Serial.read();
    if (temp == 'g'){ //Only allow the code to exit the loop if sensor 7 is tripped (g is inputed through serial)
        sensorIn = temp;
      }
    //Serial.println(temp);
    //Serial.println(sensorIn);
  }
  break;
  
  case 'g': //oscillate between Y6 and Y7 (110 => 111)
  temp = 0;
  digitalWrite(highOut, LOW);  //!E pin
  digitalWrite(highOut-1, HIGH);  //S3 pin  
  while (sensorIn == 'g'){
    digitalWrite(lowOut+1, HIGH);  //S2 pin
    digitalWrite(lowOut, LOW);  //S1 pin
    delay(500); //Delay by half a second to allow the Mux to properly change channels
    readFreq();
    printFreq();
    digitalWrite(lowOut+1, HIGH); //S3 pin
    digitalWrite(lowOut, HIGH);  //S1 pin
    delay(500); //Delay by half a second to allow the Mux to properly change channels
    readFreq();
    printFreq();
    temp = Serial.read();
    if (temp == 'h'){ //Only allow the code to exit the loop if sensor 8 is tripped (h is inputed through serial)
        sensorIn = temp;
      }
    //Serial.println(sensorIn);
  }
 break;
   
  case 'h': //Oscillate between Y7 and Y0 (111 => 000)
  digitalWrite(highOut, LOW);  //!E pin
  temp = 0;
  while (sensorIn == 'h'){
    digitalWrite(highOut-1, HIGH);   //S3 pin
    digitalWrite(lowOut+1, HIGH);  //S2 pin
    digitalWrite(lowOut, HIGH);  //S1 pin
    delay(500); //Delay by half a second to allow the Mux to properly change channels
    readFreq();
    printFreq();
    digitalWrite(highOut-1, LOW); //S3 pin
    digitalWrite(lowOut+1, LOW);  //S2 pin
    digitalWrite(lowOut, LOW);  //S1 pin
    delay(500); //Delay by half a second to allow the Mux to properly change channels
    readFreq();
    printFreq();
    temp = Serial.read();
    if (temp == 'a'){ //Only allow the code to exit the loop if sensor 1 is tripped (a is inputed through serial)
        sensorIn = temp;
      }
    //Serial.println(temp);
    //Serial.println(sensorIn);
  }
  break; 
  default:
  digitalWrite(highOut, HIGH);  //!E pin (Disables chip)
  delay(500);
  readFreq();
  printFreq();
  }
}

void readFreq(){ //Used for testing purposes, reads the time while the input is high (T/2)
  v0 = pulseIn(lowIn, HIGH, del);  //Y0
  v1 = pulseIn(lowIn+1, HIGH, del);  //Y1
  v2 = pulseIn(lowIn+2, HIGH, del);  //Y2
  v3 = pulseIn(lowIn+3, HIGH, del);  //Y3
  v4 = pulseIn(highIn-3, HIGH, del);  //Y4
  v5 = pulseIn(highIn-2, HIGH, del);  //Y5
  v6 = pulseIn(highIn-1, HIGH, del);  //Y6
  v7 = pulseIn(highIn, HIGH, del);  //Y7  
}



void printFreq (){ //Used for testing purposes, output the results to serial
  Serial.print("Time of Y0 ");
  Serial.println(v0);
  Serial.print("Time of Y1 ");
  Serial.println(v1);
  Serial.print("Time of Y2 ");
  Serial.println(v2);
  Serial.print("Time of Y3 ");
  Serial.println(v3);
  Serial.print("Time of Y4 ");
  Serial.println(v4);
  Serial.print("Time of Y5 ");
  Serial.println(v5);
  Serial.print("Time of Y6 ");
  Serial.println(v6);
  Serial.print("Time of Y7 ");
  Serial.println(v7);
}
