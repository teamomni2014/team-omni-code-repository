#include "SipHash_2_4.h"
#include "HexConversionUtils.h"
#include <RH_ASK.h>
#include <SPI.h> // Not used but needed to compile
#include <Usb.h>
#include <AndroidAccessory.h>


void send_e_stop();


//secret 16 byte key in flash memory
unsigned const char key[] PROGMEM = {0x7A, 0x71, 0x26, 0xD3, 0x4C, 0xC2, 0xCE, 0xAD,
                                0x23, 0x73, 0x75, 0x9F, 0x21, 0x47, 0x2F, 0x2A};

//for transmitting the message
RH_ASK driver;


//the android accessory to talk to
AndroidAccessory acc("Manufacturer",
		     "Model",
		     "Description",
		     "1.0",
		     "http://myapp.example.com",
		     "0000000012345678");

//setup
void setup() {
  
    //for debugging
    Serial.begin(9600);
    
    //initialize the RH_ASK driver	
    if (!driver.init()) {
         Serial.println("init failed");
    }
    
    //begin the accessory communication
    acc.powerOn();
}


//loop
void loop() {
    
    //if the accessory is connected
    if (acc.isConnected()) {
      
      Serial.println("accessory is connected.");
      
      //store communicated item in here
      byte msg[0];
      int i = -1; //write back to buffer after receiving something
      
      int len = acc.read(msg, sizeof(msg), 1);
//      Serial.print("Message length: ");
//      Serial.println(len, DEC);
//      Serial.println(msg[0]);
      
      
      if (msg[0] == 1) {

        
      } else if (msg[0] == 2) {
        
      //if the message matches up, send an emergency stop
      } else if(msg[0] == 3) {
            
        Serial.println("emergency stop");
        acc.write(&i, 1); //write back to buffer
        send_e_stop(); 
      }
      
    //if the accessory is not connected
    } else {
      
      Serial.println("not connected.");
    }
}


//send out emergency stop signals
void send_e_stop() {
  
    //keep sending the signal
    for (int j = 0; j < 2; j++) {
      
      //once initialized, variable cannot be changed
      //this will be the message we send to indicate e-stop
      //msg length must be RH_ASK_MAX_MESSAGE_LEN - 8 for the siphash
      const char *msg = "hey there";
      char msgWithHash[(strlen(msg) + 8)];
    
      //initialize with the key
      sipHash.initFromPROGMEM(key);
    
      //update the hash for each byte in msg
      for (int i = 0; i < strlen(msg); i++) {
       
         //update hash based on byte in msg
         sipHash.updateHash((byte)i); 
      }
    
      //calculate the results, stored in char[8] sipHash.result
      sipHash.finish();

      //print out the results cleanly
      Serial.print(msg);
      Serial.print(" - Hash: ");
      for (int k = 0; k < 8; k++) {
         Serial.print(sipHash.result[k]);
      }
      Serial.println();
      Serial.println();


      //put the msg and sipHash.result into one spot in memory
      memcpy(msgWithHash, msg, strlen(msg));
      memcpy(&msgWithHash[strlen(msg)], sipHash.result, 8);


      //send the message with the hash appended to the end
      driver.send((uint8_t *)msgWithHash, strlen(msgWithHash));
      driver.waitPacketSent();
      delay(2000);
    }
}
