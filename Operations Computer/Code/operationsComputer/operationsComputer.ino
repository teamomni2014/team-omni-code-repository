#include <PWM.h>
#include "SipHash_2_4.h"
#include "HexConversionUtils.h"
#include <RH_ASK.h>
#include <SPI.h> // Not used but needed to compile
#include <Usb.h>
#include <AndroidAccessory.h>

/* NOTE: The Mega has a lot of memory, so if adding a variable helps to reduce computation time, add it in. */

//-------------------------------Android Stuff--------------------------------------------//

/* [1 + Math.ceil((numSensors + 1 + 1)/8)] and is the number of bytes to write to acc buffer
one byte is added to the front for a message synchronization counter, and one bit is added
to the end to indicate if the Android or the Arduino sent it. The additional 1 is to ensure that 
message[1] is always padded in the front with 0s */
int const messageSize = 5; //TODO placeholder 
byte lastMessageSent[messageSize];
byte androidSyncCounter = 0x01;

/* Used to keep the message sending synchronized (there's a danger of overwriting) 
It's a byte, so only writes from 0 to 255, then wraps around back to 0 */
byte messageSentCounter = 0; 

/* the Android accessory to talk to. No changes should be made to this or the device
won't be able to connect. */
AndroidAccessory acc("Manufacturer",
			"Model",
			"Description",
			"1.0",
			"http://myapp.example.com",
			"0000000012345678");

//-------------------------------RF Stuff--------------------------------------------//

//secret 16 byte key for encrypted serial communication. Don't change!
unsigned const char key[] PROGMEM = {0x7A, 0x71, 0x26, 0xD3, 0x4C, 0xC2, 0xCE, 0xAD,
										0x23, 0x73, 0x75, 0x9F, 0x21, 0x47, 0x2F, 0x2A};

//for transmitting messages to vehicles
RH_ASK driver;

//-------------------------------Block Stuff--------------------------------------------//

//frequency output in Hz
int32_t const freq = 1; 

//number of blocks and sensors in the arena
int const numBlocks = 10; //TODO placeholder
int const numSensors = 5; //TODO placeholder

//the pin that enables the DeMUX/MUX chip. It's !E
int const enablePin = 25; //TODO placeholder

//the pin that determines if the chip is DeMUX or MUX 
int const zPin = 11; //TODO placeholder

//the selector pins on the MUX/DeMUX
int const selectorPins[] = {22, 23, 24}; //TODO placeholder

/* Stores the selector bit values for the blocks to be powered.
The end of the array is indicated by a -1 value. The size of this array
is equal to the number of blocks. */
int selectorBitsToPower[numBlocks];


//-------------------------------Setup and Loop----------------------------------------//


void setup() {

	//initialize all timers except for 0, to save time keeping functions
	InitTimersSafe(); 

	//begin serial communications
	Serial.begin(9600);


	/*------- Block control setup -------*/

	//initialize lastMessageSent
	lastMessageSent[messageSize - 1] = 0x01;

	//until the Android says to, don't power any blocks
	selectorBitsToPower[0] = -1;

	//if frequency was set, print success
	if (!SetPinFrequencySafe(zPin, freq)) {
		Serial.println("init failed 1");
	}


	/*------- Pin mode setup -------*/

	//enable the MUX/DeMUX chip by writing low - the pin is !E
	digitalWrite(enablePin, LOW); 

	//configure PWM output pin
	pinMode(zPin, OUTPUT);

	//configure all selector pins as outputs
	for (int i = 0; i < sizeof(selectorPins); i++) pinMode(selectorPins[i], OUTPUT);


	/*------- RH comm and Android comm setup -------*/

	//initialize the RH_ASK driver  
	if (!driver.init()) {
		Serial.println("init failed 2");
	}

	//begin the accessory communication
	acc.powerOn();
}


void loop() {

	/*------- Control the DeMux -------*/
	// muxBlockControlTest(); //TODO

	/*------- Check which blocks to turn on -------*/
	getAndroidMessageTest(); //TODO

	/*------- Check sensors and send update to Android -------*/
	//checkSensorsTest(); //TODO
}


//-------------------------------Block Control----------------------------------------//


/** Turns on the block sections based on the information retrieved from
getAndroidMessage() */
void muxBlockControl() {


	//set the chip to output
	pinMode(zPin, OUTPUT);
	pwmWrite(zPin, 131); //set pwm pin to have a 50% duty cycle (131 is 50% plus extra to adjust)


	//TODO use the selectorBitsToPower array to figure out which blocks to power on
	//TODO use Grey code so that the enabler pin doesn't need to keep being toggled
	//TODO need some algorithm to optimize this
}


/** Check all of the block sensors and send a message to the accessory 
Note: using bits for everything instead of a byte per sensor speeds up the writing */
void checkSensors() {

	int bitIndex = 0;
	int sensorVoltage;
	byte message[messageSize]; 


	//get input from the chip's pin
	pinMode(zPin, INPUT);

	//put a 1 at the end to indicate that the Arduino sent it
	message[messageSize - 1] = 0x01;

	//for each block section sensor in the arena
	for (int i = 0; i < numSensors; i++) {

		/* For each selector, digital write to selector pins.
		The selectors are checked least to most significant */
		for (int j = 0; j < sizeof(selectorPins); j++) {

			//uses i to decide which selectors to write to
			if ((i >> j) & 0x01) digitalWrite(selectorPins[j], HIGH);
			else digitalWrite(selectorPins[j], LOW);
		}

		//then read from the MUX
		sensorVoltage = analogRead(zPin);

		bitIndex++; //bitIndex will always be incremented at least once

		//if the sensorVoltage is over a certain threshold (20 was chosen arbitrarily)
		if (sensorVoltage > 20) {

			// TODO figure out which vehicle, then or it with 0, 1, 2, or 3
			int vehicleID = map(sensorVoltage, 0, 1023, 0, 3);

			//complicated math things to change desired bits
			message[messageSize - 1 - (bitIndex / 8)] |= (0x01 << (bitIndex % 8));
			message[messageSize - 1 - ((bitIndex + 2) / 8)] |= ((vehicleID / 2) << ((bitIndex + 2) % 8));
			message[messageSize - 1 - ((bitIndex + 1) / 8)] |= ((vehicleID % 2) << ((bitIndex + 1) % 8));
		
			bitIndex += 2; //to account for the 2 vehicle bits following the 1
		}
	}

	/* Only write to the buffer if there's a difference between the last message
	because of how long writing to the buffer takes. */
	if (!arrayEqual(message, lastMessageSent, messageSize)) {

		//append the synchronization counter to the front 
		message[0] = messageSentCounter;

		//send the byte message 
		sendAndroidMessage(message);

		//make this the new reference message
		memcpy(lastMessageSent, message, messageSize);
	}
}


//-------------------------------Prototype 2 Testing----------------------------------------//


//just for prototype 2 testing purposes
bool loadingDock = false;


/** For the purposes of Prototype 2 testing */
void muxBlockControlTest() {

	//send pwm to the chip
	pinMode(zPin, OUTPUT);
	pwmWrite(zPin, 131); //set pwm pin to have a 50% duty cycle (131 is 50% plus extra to adjust)

	digitalWrite(selectorPins[2], HIGH); 
	digitalWrite(selectorPins[0], HIGH); 
	digitalWrite(selectorPins[1], HIGH); //111 - Y7

	//iterate through this 5 times
//	for (int i = 0; i < 5; i++) {
//
//		digitalWrite(selectorPins[1], HIGH); //111 - Y7
//		delay(20);
//		digitalWrite(selectorPins[2], LOW); //011 - Y3
//		delay(20);
//		digitalWrite(selectorPins[0], LOW); //010 - Y2
//		delay(20);
//		digitalWrite(selectorPins[1], LOW); //000 - Y0
//		delay(20);
//
//		if (loadingDock) {
//			digitalWrite(selectorPins[2], HIGH); //100 - Y4
//			delay(20);
//			digitalWrite(selectorPins[0], HIGH); //101
//		} else {
//			digitalWrite(selectorPins[0], HIGH); //001 - Y1
//			delay(20);
//			digitalWrite(selectorPins[2], HIGH); //101
//		}
//	}
}


/** For the purposes of Prototype 2 testing */
void checkSensorsTest() {

	int sensorVoltage;
	byte message[messageSize]; 

	//put a 1 at the end to indicate that the Arduino sent it
	message[messageSize - 1] = 0x01;

	//the chip is now taking sensor input
	pinMode(zPin, INPUT);

	//check Y5 - 101
	digitalWrite(selectorPins[0], HIGH);
	digitalWrite(selectorPins[1], LOW);
	digitalWrite(selectorPins[2], HIGH);
	delay(20);
	sensorVoltage = analogRead(zPin);
	if (sensorVoltage > 20) message[messageSize - 1] |= 0x02; //second to last bit

	//check Y6 - 110
	digitalWrite(selectorPins[0], LOW);
	digitalWrite(selectorPins[1], HIGH);
	delay(20);
	sensorVoltage = analogRead(zPin);
	if (sensorVoltage > 20) message[messageSize - 1] |= 0x04; //third to last bit

	/* Only write to the buffer if there's a difference between the last message
	because of how long writing to the buffer takes. */
	if (message[messageSize - 1] != lastMessageSent[messageSize - 1]) {

		//append the synchronization counter to the front 
		message[0] = messageSentCounter;

		//send the byte message 
		sendAndroidMessage(message);

		//make this the new reference message
		memcpy(lastMessageSent, message, messageSize);
	}
}


/** For the purposes of Prototype 2 testing */
void getAndroidMessageTest() {

	//if the accessory is connected
	if (acc.isConnected()) {

		//store communicated item in message
		byte message[messageSize];

		/* Check if a message has been sent from the Android device and
		store the message in the byte array. Not sure what the 1 does. */
		acc.read(message, messageSize, 1);

                for (int i = 0; i < messageSize; i++) {
                Serial.print(message[i]);
                Serial.print(" ");
                }
                Serial.println();

		//if the message was sent from the Android (last bit of message is 0)
		if (!(message[messageSize - 1] & 0x01)) {

			//if the message isn't the same as the last one received
			if (message[0] != androidSyncCounter) {
				//if message[1] is not padded with 0s
				if(message[1] == 0xff) { 
					//send an e-stop
					send_e_stop(); 

				} else {
					//make this the last message received
					androidSyncCounter = message[0];

					//check if the second to last bit in the message is 1
					//this toggles which of the two blocks is powered
					if ((message[messageSize - 2] >> 1) & 0x01) {
						loadingDock = true; //if the second to last bit is 1
					} else {
						loadingDock = false; //if the second to last bit is 0
					}
				}
			}
		}
	} else {
		Serial.println("Not connected to Android device."); //testing purposes
	}
}


//-------------------------------Android Control----------------------------------------//


/** Called if a sensor is tripped to tell the Android that a vehicle
is entering/leaving a block. */
void sendAndroidMessage(byte *message) {

	//if the accessory is connected
	if (acc.isConnected()) {

		//write the message to the buffer *THIS TAKES A LOT OF TIME*
		acc.write(message, messageSize);

		//increment message synchronization counter
		messageSentCounter++;

	} else {
		// Serial.println("Not connected to Android device."); //testing purposes
	}
}


/** Called on every tick. This checks if an e-stop has been called
and to determine which blocks to power. */
void getAndroidMessage() {

	//if the accessory is connected
	if (acc.isConnected()) {

		//store communicated item in message
		byte message[messageSize];

		/* Check if a message has been sent from the Android device and
		store the message in the byte array. Not sure what the 1 does. */
		acc.read(message, messageSize, 1);


		//if the message was sent from the Android (last bit of message is 0)
		if (!(message[messageSize - 1] & 0x01)) {

			//if the message isn't the same as the last one received
			if (message[0] != androidSyncCounter) {

				//if message[1] is not padded with 0s //TODO this is not implemented in the actual writeToBuffer code
				if(message[1] == 0xff) {

					//send an e-stop
					send_e_stop(); 

				} else {

					//make this the last message received
					androidSyncCounter = message[0];

					/* Translate the message the ops computer sent to update which blocks should be on */
					int bitLocation = 1; //one because the first bit is the Android identifier 0
					int arrayLocation = 0; //used to keep track of where in selectorBitsToPower array we are //TODO may not be needed now

					//check the message for each block
					for (int i = 0; i < numBlocks; i++) {

						//if the bit in the message is 1. The check goes backwards through the byte array
						if ((message[messageSize - 1 - (bitLocation / 8)] >> (bitLocation % 8)) & 0x01) {

							//save this selector combination in the array to be powered
							selectorBitsToPower[arrayLocation] = i;
							arrayLocation++;
						}
						bitLocation++;
					}

					//check vehicle speeds
					for (int i = 0; i < 4; i++) {
						//TODO figure out a good way to do this

						//if (???) send_speed(int);
					}

					//if the selectorBitsToPower array isn't full, end the array with -1
					if (arrayLocation != numBlocks) selectorBitsToPower[arrayLocation] = -1; //TODO this will change now
				}
			}
		}
	} 
}


//-------------------------------E-Stop and Speed----------------------------------------//


/** Change the vehicle's speed */
void send_speed(int speed) {

	const char *message = str(speed); 
	char msgWithHash[strlen(message) + 8];

	//initialize with the key
	sipHash.initFromPROGMEM(key);
	
	//update hash based on byte in message
	sipHash.updateHash((byte)str(speed)); 

	//calculate the results, stored in char[8] sipHash.result
	sipHash.finish();

	//put the message and sipHash.result into one spot in memory
	memcpy(msgWithHash, message, strlen(message));
	memcpy(&msgWithHash[strlen(message)], sipHash.result, 8);

	//send the message with the hash appended to the end
	driver.send((uint8_t *)msgWithHash, strlen(msgWithHash));
	driver.waitPacketSent();
}


/** Send out emergency stop signals */
void send_e_stop() {

	/* Keep sending the signal to ensure vehicles get it
	20 was arbitrarily chosen */
	for (int j = 0; j < 20; j++) {

		/* This will be the message we send to indicate e-stop. If you 
		update the message, update the length in the vehicle code
		message length must be RH_ASK_MAX_MESSAGE_LEN - 8 for the SipHash */
		const char *message = "e stop"; //TODO placeholder 
		char msgWithHash[strlen(message) + 8];

		//initialize with the key
		sipHash.initFromPROGMEM(key);

		//update the hash for each byte in message
		for (int i = 0; i < strlen(message); i++) {

			//update hash based on byte in message
			sipHash.updateHash((byte)message[i]); 
		}

		//calculate the results, stored in char[8] sipHash.result
		sipHash.finish();

		//put the message and sipHash.result into one spot in memory
		memcpy(msgWithHash, message, strlen(message));
		memcpy(&msgWithHash[strlen(message)], sipHash.result, 8);

		//send the message with the hash appended to the end
		driver.send((uint8_t *)msgWithHash, strlen(msgWithHash));
		driver.waitPacketSent();
		delay(50); //wait for the vehicle to receive and decode
	}

	//TODO reset now?
}


//-------------------------------Helper methods----------------------------------------//


/** Helper method used to compare message arrays. This ignores the first byte because
the first byte is the synchronization counter. */
int arrayEqual(byte *array1, byte *array2, int length) {

	//compare each element in the arrays (except for the first)
	for (int i = 1; i < length; i++) {

		//if the two elements differ, return 0
		if (array1[i] != array2[i]) {
			return 0;
		}
	}
	return 1;
}