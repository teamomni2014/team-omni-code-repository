
/*
  Inductive Guidance Prototype 1
  
  This program is designed to demonstrate all functions of the inductive guidance system using a single arduino.
  Two variables Motor_L and Motor_R represent the amplitude of the left and right motors on the vehicle. 
  An amplitude of 5 V corresponds to full speed while 0 V corresponds with a stop command.
  The amplitude of each motor uses a running average of the need to turn left or right. 
  
*/

#include <PWM.h>
#include <Stepper.h>

int32_t frequency = 13200; //frequency output (in Hz)

int leftSensorPin = A0;    // select the input pin for the left inductor
int rightSensorPin = A4;    // select the input pin for the right inductor
int motor_L = 2;     // select the pin for the left motor
int motor_R = 4;     // select the pin for the right motor
float sensorAverageL = 0;  // variable to store the previous values for the sensor to allow for smooth turns hopefully.
float sensorAverageR = 0;  // variable to store the previous values for the sensor to allow for smooth turns hopefully.
float difference = 0;
int sensor_L = 0; //variable to store motor amplitude
int sensor_R = 0; //variable to store motor amplitude
int reading = 0;
int readingHigh = 0;
int readingLow = 1023;
int update_rate = 100; // delay for 50 ms between readings
float alpha = 0.004*update_rate;

int speedL = 0;
int speedR = 0;

const unsigned char PS_8 = (1 << ADPS1) ;
const unsigned char PS_128 = (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);

const int stepsPerRevolution = 200;
Stepper myStepper(stepsPerRevolution, 8,11,12,13); 

void setup()
{
  // initialize the serial port:
  Serial.begin(9600);
  pinMode(9,OUTPUT);
  pinMode(10,OUTPUT);
  digitalWrite(9,HIGH);
  digitalWrite(10,HIGH);
  
  //initialize all timers except for 0, to save time keeping functions
  InitTimersSafe(); 

  //sets the frequency for the specified pin
  bool success = SetPinFrequencySafe(3, frequency);
  
  Serial.begin(9600);
  //if the pin frequency was set successfully, turn pin 13 on
  if(success) 
  {  
    //Serial.println("Success in setup");
    //turn on the Arduino LED
    pinMode(13, OUTPUT);
    digitalWrite(13, HIGH);    
  }
  
  pinMode(leftSensorPin, INPUT);
  pinMode(rightSensorPin, INPUT);
  
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  
  //pinMode(7, INPUT);
  pinMode(motor_L, OUTPUT); //set motor output pins
  pinMode(motor_R, OUTPUT);
  pinMode(3,OUTPUT);
  
  ADCSRA &= ~PS_128;  // remove bits set by Arduino library
  ADCSRA |= PS_8;    // set our own prescaler to 8
 /* 
   pinMode(7,INPUT);
   pinMode(6,INPUT);
    sensor_L = 1023;
    sensor_R = 1023;
  //calibrating right and left sensors to be equal
  while(digitalRead(7) == LOW)
  {
    readingHigh = 0; //initialize at oposite end 
    readingLow = 1023;
    reading = analogRead(rightSensorPin);
    delay(10);
        for (unsigned int j = 0; j < 100; j++) 
    {  
      reading = analogRead(rightSensorPin);
     if(reading > readingHigh)
     {
       readingHigh = reading;
     }
      if(reading < readingLow)
   {
     readingLow = reading;
   }
    } 
     if((readingHigh - readingLow) < sensor_R)
     {
     sensor_R = readingHigh - readingLow;
      Serial.print("Sensor R: ");
      Serial.println(sensor_R);
     }
    
  }
   readingHigh = 0; //initialize at oposite end 
    readingLow = 1023;
  while(digitalRead(6) == LOW)
  {
    
    readingHigh = 0; //initialize at oposite end 
    reading = analogRead(leftSensorPin);
    delay(20);
    for (unsigned int k = 0; k < 100; k++) 
    {  
      readingHigh = 0;
      readingLow = 1023;
        for (unsigned int j = 0; j < 100; j++) 
    {  
      pwmWrite(9, 131);
      
      reading = analogRead(leftSensorPin);
      delayMicroseconds(1);

     delayMicroseconds(10);
     if(reading > readingHigh)
     {
       readingHigh = reading;
     }
      if(reading < readingLow)
   {
     readingLow = reading;
   } 
    }
     Serial.print(readingHigh-readingLow);
     Serial.print(",");
    }
    if((readingHigh - readingLow) < sensor_L)
     {
     sensor_L = readingHigh - readingLow;
      //Serial.print("Sensor L: ");
      //Serial.println(sensor_L);
     }
     
  
/*
    LR_calibration = (float)sensor_L/sensor_R;
    delay(10);
    Serial.println(LR_calibration);
  */
}

void loop()
{
  pwmWrite(3, 131); //continue to write the output frequency
  /*
  sensor_L = analogRead(leftSensorPin);  //get value from left inductor
  sensor_R = analogRead(rightSensorPin); //get value from right inductor
  
  sensorAverageL = alpha * sensor_L + (1 - alpha) * sensorAverageL;
  sensorAverageR = alpha * sensor_R + (1 - alpha) * sensorAverageR;
  
  Serial.print("Sensor L: ");
  Serial.println(sensor_L);
  Serial.print("Sensor R: ");
  Serial.println(sensor_R);
  
  if(sensor_L > sensor_R)
  {
    //Serial.print("Motor L: ");
    //Serial.println(5 *  (sensorAverageR/sensorAverageL));
    //Serial.print("Motor R: ");
    //Serial.println(HIGH);
    
    digitalWrite(motor_L,  5 *  (sensorAverageR/sensorAverageL));
    digitalWrite(motor_R , HIGH);
  }
  else
  {
    //Serial.print("Motor L: ");
    //Serial.println(HIGH);
    //Serial.print("Motor R: ");
    //Serial.println(5 *  (sensorAverageL/sensorAverageR));
    
    digitalWrite(motor_R , 5 *  (sensorAverageL/sensorAverageR));
    digitalWrite(motor_L , HIGH);
  }
  */
  //Serial.println("Starting Sequence");
  //initialized ADC for read pins, allowing time for switching
  sensor_L = 0;
  sensor_R = 0;
  readingHigh = 0; //initialize at oposite end 
  readingLow = 1023;
  reading = analogRead(rightSensorPin);
  delay(10);
      for (unsigned int j = 0; j < 100; j++) 
  {  
    reading = analogRead(rightSensorPin);
    delayMicroseconds(1);
    if(reading < readingLow)
   {
     readingLow = reading;
   } 
   if(reading > readingHigh)
   {
     readingHigh = reading;
   }

  }
  sensor_R = (readingHigh - readingLow);
  if(sensor_R < 220)
  {
    sensor_R = 0;
  }
  reading = analogRead(leftSensorPin);
   readingHigh = 0; //initialize at oposite end 
  readingLow = 1023;
  delay(10);
   for (unsigned int j = 0; j < 100; j++) 
  {  
    reading = analogRead(leftSensorPin);
    delayMicroseconds(1);
    if(reading < readingLow)
   {
     readingLow = reading;
   } 
   if(reading > readingHigh)
   {
     readingHigh = reading;
   }

  }
  sensor_L = (readingHigh - readingLow);
  if(sensor_L < 220)
  {
    sensor_L = 0;
  }
 // sensor_L = (sensor_L - 15)*100;
 // sensor_R = (sensor_R - 15)*100;
    //sensor_R += analogRead(A5);
     //freq += analogRead(A2);
     //freq += 500000/pulseIn(7, HIGH, 25000); 
    //delay(10);
    //sensor_L -= analogRead(A3);
    //delay(10);
    //sensor_R -= analogRead(A3);
  //voltage = (500*freq)/(4096);
  /*
   Serial.print("Sensor L: ");
   Serial.println( sensor_L);
   Serial.print("Sensor R: ");
   Serial.println( sensor_R);
   /*
 // delay(10);
  /*
  Serial.write(27);       // ESC command
  Serial.print("[2J");    // clear screen command
  Serial.write(27);
  Serial.print("[H");
  */
//  sensor_L = 50*sensor_L;
  //sensor_R = 50*sensor_R;
  sensorAverageL = (alpha)*((float)sensor_L/600) + ((1 - alpha) * sensorAverageL);
  delay(1);
  sensorAverageR = (alpha)*((float)sensor_R/600) + ((1 - alpha) * sensorAverageR);
  delay(1);
 
  speedL =(int)(255*(1-sensorAverageL));
  delay(1);
  speedR =(int)(255*(1-sensorAverageR));
   //Serial.print("Motor R: ");
   //Serial.println(1-sensorAverageR);
   //Serial.print("Motor L: ");
   //Serial.println(1-sensorAverageL);
   Serial.write('L');
   Serial.println(speedL);
   delay(10);
   Serial.write('R');
   Serial.println(speedR);
   
    pwmWrite(9, speedR/3);
    
    pwmWrite(10, speedL/3);
   
   delay(update_rate);
  //difference = sensorAverageL - sensorAverageR;
 /*
  if(difference > 0 )
 {
   digitalWrite(motor_L , 1-sensorAverage_L);
   digitalWrite(motor_R , HIGH);
   /*
   Serial.print("Motor R: ");
    Serial.println(5);
        Serial.print("Motor L: ");
    Serial.println(5 *  (sensorAverageR/sensorAverageL));
    
 } 
 else if(difference < 0)
 {
   digitalWrite(motor_R , 5 *  (sensorAverageL/(sensorAverageR)));
   digitalWrite(motor_L , HIGH);
   /*
   Serial.print("Motor R: ");
    Serial.println(5 *  (sensorAverageL/(sensorAverageR*LR_calibration)));
        Serial.print("Motor L: ");
    Serial.println(5);
    
 } 
 else
 {
   digitalWrite(motor_L , HIGH);
   digitalWrite(motor_R , HIGH);
   
   Serial.print("Motor L: ");
    Serial.println(5);
   Serial.print("Motor R: ");
    Serial.println(5);
    
 }
    */

  /*  
  Serial.print("Sensor L avg: ");
  Serial.println(sensorAverageL);
  Serial.print("Sensor R avg: ");
  Serial.println(sensorAverageR);
  /*
    if(sensor_L > sensor_R)
  {
    Serial.println("Motor L: ");
    //Serial.println(5 *  (sensorAverageR/sensorAverageL));
    //Serial.print("Motor R: ");
    //Serial.println(HIGH);
    
   // digitalWrite(motor_L,  5 *  (sensorAverageR/sensorAverageL));
    //digitalWrite(motor_R , HIGH);
  }
  else
  {
    //Serial.print("Motor L: ");
    Serial.println(HIGH);
    Serial.println("Motor R: ");
    //Serial.println(5 *  (sensorAverageL/sensorAverageR));
    
   // digitalWrite(motor_R , 5 *  (sensorAverageL/sensorAverageR));
    //digitalWrite(motor_L , HIGH);
  }
  */
}
