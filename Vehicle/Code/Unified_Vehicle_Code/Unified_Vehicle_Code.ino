/*  Unified Vehicle Code
*  This code is intended to be used as the only program running on the vehicle computer for Project rides,
*  it integrates the inductive line following, motor output, obstacle detection, power control, and RF reciever functions. 
*/
#include "SipHash_2_4.h"
#include "HexConversionUtils.h"
#include <RH_ASK.h>
#include <SPI.h> // Not used but needed to compile
#include <PID_v1.h>


//pin to disconnect power for E-stop
int powerPin = 2;

////////////////////////
//**PID CONTROL VARS**//
////////////////////////
//double kp = 0.87;
//double ki = 1.05;
//double kd = 0.12;
//double kp =  3.5;
//double ki =  0.0;
//double kd =  70.0;
double kp =  0.85;
double ki =  0.3;
double kd =  30.0;
///////////////////////
//**RF RECIEVE VARS**//
///////////////////////

int are_equal(unsigned char *array1, unsigned char *array2);
void e_stop();

//secret 16 byte key in flash memory
unsigned const char key[] PROGMEM = {0x7A, 0x71, 0x26, 0xD3, 0x4C, 0xC2, 0xCE, 0xAD,
                                0x23, 0x73, 0x75, 0x9F, 0x21, 0x47, 0x2F, 0x2A};

//for transmitting the message
RH_ASK driver;

//how big the agreed upon message is
int sizeOfMessage = 9;


/////////////////////
//**OBSTACLE VARS**//
/////////////////////
int obstacleSensorPin = A1;                   // analog pin used to connect the sharp sensor
int obstacleDistance = 0;            // value for the obstacle detection, higher means closer
float obstacleScalar = 1;
  float slowSpeed = 0.5;
float beta = 0.04; //used for moving average


///////////////////////////////
//**INDUCTIVE GUIDANCE VARS**//
///////////////////////////////
int leftSensorPin = A0;    // select the input pin for the left inductor
int rightSensorPin = A4;    // select the input pin for the right inductor
int centerSensorPin = A2;  //select the input pin for the center inductor
int motor_L = 9;     // select the pin for the left motor
int motor_R = 10;     // select the pin for the right motor
double sensorAverageL = 1;  // variable to store the previous values for the sensor to allow for smooth turns hopefully.
double sensorAverageR = 1;  // variable to store the previous values for the sensor to allow for smooth turns hopefully.
float difference = 0;
int sensor_L = 0; //variable to store sensor amplitude
int sensor_R = 0; //variable to store sensor amplitude
int sensor_C = 0; //variable to store sensor amplitude

//sensor calibration variables
int L_max = 700;
int L_min = 100;
int R_max = 700;
int R_min = 100;
int C_max = 700;
int C_min = 100;
///////////////////////////////

int reading = 0;
int readingHigh = 0;
int readingLow = 1023;
int update_rate = 00; // delay for 50 ms between readings
long lastUpdate = 0; //time of the last update for the motors.
float alpha = 0.3;

//PID variables
double Setpoint, Input, Output;

//Specify the links and initial tuning parameters
PID pidControl(&Input, &Output, &Setpoint,kp,ki,kd, REVERSE);

int buffer[100];
int lastRead = 0;      //amplitude of last read value
long lastPeak = 0; //time in millis of last peak
int peaks = 0; //# of peaks found in loop
boolean idPositive = true; //if we were able to find the frequency we are looking for
boolean low = false; //if the signal is decreasing (falling edge)

int speedL = 0; //value to be written to left motor speed control
int speedR = 0; //value to be written to right motor speed control

const unsigned char PS_8 = (1 << ADPS1) ; //constant for prescalar of 8
const unsigned char PS_128 = (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); //default prescalar of 128

int frequencyPin = 5; //might be old idk
long freq = 0; //frequency detected on arena wire

//Motor Shield Setup
int pinI1=8;//define I1 interface
int pinI2=11;//define I2 interface 
int pinI3=12;//define I3 interface 
int pinI4=13;//define I4 interface

long last_path_time = 0;
boolean on_path = false;

int speed_offset = 0;
int maxSpeed = 90;
boolean last_left = false;

int last_case = 6;
int max_avg = 700;


void setup()
{
  // initialize the serial port:
  Serial.begin(9600);
  
  //pin to disconnect power for E-stop
  pinMode(powerPin, OUTPUT);
  digitalWrite(powerPin, HIGH);
  
  ////////////////////////
  //**RF RECIEVE SETUP**//
  ////////////////////////
  
  if (!driver.init())  //initialize the RH_ASK driver
  {
    Serial.println("init failed");
  }
  //////////////////////
  //**OBSTACLE SETUP**/
  //////////////////////
  
  pinMode(obstacleSensorPin, INPUT);
 // attachInterrupt(obstacleSensorPin, obstacle, CHANGE);
  
  
  /////////////////////////////////
  //**INDUCTIVE GUIDAWNCE SETUP**/
  /////////////////////////////////
  
  pinMode(leftSensorPin, INPUT);
  pinMode(rightSensorPin, INPUT);
  pinMode(centerSensorPin, INPUT);
  
  
  //pinMode(9, OUTPUT);
  //pinMode(10, OUTPUT);
  
  //Motor Shield Setup
  pinMode(pinI1,OUTPUT);
  pinMode(pinI2,OUTPUT);
  pinMode(motor_L, OUTPUT); //set motor output pins
  pinMode(pinI3,OUTPUT);
  pinMode(pinI4,OUTPUT);
  pinMode(motor_R, OUTPUT);
  
  ADCSRA &= ~PS_128;  // remove bits set by Arduino library
  ADCSRA |= PS_8;    // set our own prescaler to 8
  
  //delay(500);
  //digitalWrite(powerPin, HIGH);
  Input = 128;
  Setpoint = 255;

  //turn the PID on
  pidControl.SetMode(AUTOMATIC);
  pidControl.SetOutputLimits(-512, 512); 
}

void obstacle()
{
  analogRead(obstacleSensorPin); 
  delay(5);
  obstacleDistance = analogRead(obstacleSensorPin); 
}

void updateObstacle()
{
  obstacle();
  //Serial.println(obstacleDistance);
  if(obstacleDistance < 130) //all clear, full speed ahead. 
  {
    obstacleScalar = (1*beta)+((1-beta)*obstacleScalar);
    
  }  
  else if(obstacleDistance > 130 && obstacleDistance < 240) //obstacle detected a bit ahead, slow down to allow it to pass
  {
    obstacleScalar = (slowSpeed*beta)+((1-beta)*obstacleScalar);
    delay(1);
  }  
  else if(obstacleDistance > 240) //about to hit stop immediately
  {
    obstacleScalar = 0;    
  }
    /*
    Serial.print("Distance: ");
    Serial.println(obstacleDistance);
    /*
    Serial.print("Scalar: ");
    Serial.println(obstacleScalar );
    */
    //Serial.println( analogRead(obstacleSensorPin));
}

//function called when e-stop signal recieved, cuts power to system
void e_stop()
{
  //write low to unlatch the power relay, causing all power to stop.
 // digitalWrite(powerPin, LOW);
}


//compare two character arrays to verify source of RF signal
int are_equal(unsigned char *array1, unsigned char *array2) {
  
    //compare each byte in the two char arrays
    for (int i = 0; i < 8; i++) {
      
      //if the two characters are different, return 0
      if (array1[i] != array2[i]) return 0; 
    }
    
    //return true if there are no characters different
    return 1;
}

//used to get messages from the operations computer. 

void updateRF()
{
    //array to hold the message received in
    unsigned char msg[(sizeOfMessage + 8)];
    unsigned char msgLength = sizeOfMessage + 8;
    unsigned char *subsetArray = &msg[sizeOfMessage]; //for last 8 bytes

    //if message received correctly, does not block
    if (driver.recv(msg, &msgLength))
    {
	// Message with a good checksum received, dump it.
	//driver.printBuffer("Got:", msg, msgLength);
        
        //initialize with the key
        sipHash.initFromPROGMEM(key);
    
        //update the hash for each byte in msg not including hash
        for (int i = 0; i < sizeOfMessage; i++) {
       
           //update hash based on byte in msg
           sipHash.updateHash((byte)i); 
        }
    
        //calculate the results, stored in char[8] sipHash.result
        sipHash.finish();

        //print out the results cleanly
        for (int y = 0; y < sizeOfMessage; y++) {
           Serial.print((char)msg[y]); 
        }
        Serial.print(" - Hash: ");
        for (int k = 0; k < 8; k++) {
           Serial.print(subsetArray[k], DEC);
        }
        Serial.println();


        //compare the last 8 bytes in msg to sipHash.result
        //if the two are equal, call an emergency stop
        if (are_equal(subsetArray, sipHash.result)) {
            
            Serial.println("The two hashes match!");
            Serial.println();
            
            //call an e-stop
            e_stop();
        }
    }
}

//Pathfollowing code
void updateMotors()
{
  //reset all frequency detection variables
  idPositive = false;
  freq = 0;
  lastRead = 0;
  peaks = 0;
  sensor_L = 0;
  sensor_R = 0;
  sensor_C = 0;
   //initialize edges at oposite end
  readingHigh = 0; 
  readingLow = 1023; 
  reading = analogRead(rightSensorPin);     //initialized ADC for read pins, allowing time for switching
  //delay(10); //delay to account for ADC switching
  
  lastPeak = micros(); //setting start time of readings for frequency calculation
  
  //take 100 readings from the right sensor and store them
  for (unsigned int j = 0; j < 100; j++) 
  {  
    buffer[j] = analogRead(rightSensorPin);
  }
  
  lastPeak = micros() - lastPeak; //taking time after readins minus start time, giving time elapsed for readings.
  
  //for each of the readings we took
  for (unsigned int j = 0; j < 100; j++) 
  {
    //set a local variable to the current reading
    reading = buffer[j];
   
   if(reading < readingLow) //if this is the lowest reading we have ever seen, set the low point for amplitude calculation
   {
     readingLow = reading;
   } 
   else if(reading > readingHigh)//if this is the highest reading we have ever seen, then set high point for amplitude calculation
   {
     readingHigh = reading;
   }
   
   if(low) //if the signal is overall dropping
   {
      if(reading < lastRead) //if the signal is still going down
     {
       lastRead = reading; //set this as the last read and continue
     }
     else
     { 
       if(reading > (lastRead + 50)) //if the signal is rising again, and is past the noise threshhold of 50 
       {
         low = false; //then the signal is now rising
         lastRead = 0; //and we reset the last reading to the lowest possible reading
       }
     }
   }
   else //if the signal is overall rising
   {
     if(reading > lastRead) //and the signal is continuing to rise
     {
       lastRead = reading; //set last read keep rolling
     }
     else
     {
       if(reading < (lastRead - 50)) //if the signal is lower than before, and extends past the noise threshold
       {
         peaks++; //then we have passed a peak, so increment the counter
         low = true; //and the signal is now dropping
         lastRead = 1023; //reset last read to highest possible value
       }
     }
   }
  }
  
  //FREQUENCY CALCULATIONS//
  freq = (1000000*peaks)/(lastPeak); //take the time elapsed for all readings (lastPeak) converted to seconds, and divide the number of peaks seen by that value to get Hz 
  if( freq > 10000 && freq < 16000) //if the frequency is within 3 kHz of the expected value
  {
    idPositive = true; //then we can say that we have found the path on the right sensor
//       Serial.println();
//   Serial.print("freq");
//   Serial.println(freq);
  }
    else
  {
//    Serial.println();
//    Serial.print("freq");
//    Serial.println(freq);
   idPositive = false;
  }
  
  //set the amplitude of the right sensor for this pass
  sensor_R = (readingHigh - readingLow);
  
  //if the sensor amplitude is not past a minimum threshhold
  if(sensor_R < 150)
  {
    if(sensor_R < R_min) //and it is lower than the lowest amplitude we have ever seen
    {
      R_min = sensor_R; //then set the low point for calibration
      
    }
    sensor_R = R_min; //then our value is effectively zero, to account for noise, so set to low calibration point
  }
  if(sensor_R > R_max) //if the amplitude is higher than anything we have seen
  {
     R_max = sensor_R; //then set the high calibration point
  }
 

  peaks = 0;   //reset the peaks we have seen to 0
  lastRead = 0;//reset the last reading value
  reading = analogRead(leftSensorPin); //trigger a reading on the left sensor to start ADC switch process
  readingHigh = 0; //initialize at oposite end 
  readingLow = 1023; //see above
  //delay(10); //wait for ADC to finish switching
  lastPeak = micros(); //set time of reading start\
  
  //take 100 readings from the left sensor and store the values  
  for (unsigned int j = 0; j < 100; j++) 
  {  
    buffer[j] = analogRead(leftSensorPin);
  }
  
  lastPeak = micros() - lastPeak; //set time elapsed for readings
  
  //for each of the readings we took
  for (unsigned int j = 0; j < 100; j++) 
  {  
    reading = buffer[j]; //set a local variable to hold value
    
    if(reading < readingLow) //if the reading is lower than any we have seen this pass
    {
      readingLow = reading; //set the low point for the amplitude 
    } 
    else if(reading > readingHigh) //otherwise, if the reading is higher than any this pas
    {
     readingHigh = reading; //set he high point for the amplitude
    }
   
   if(low) //if the signal is dropping overall
   {
      if(reading < lastRead) //and the current reading is lower than the last reading
     {
       lastRead = reading; //then set this reading as the last reading and move on
     }
     else //otherwise
     { 
       if(reading > (lastRead + 50)) //if the signal is rising more than the noise threshold
       {
         low = false; //then the signal is no longer dropping
         lastRead = 0; //reset the last read
       }
     }
   }
   else //if the signal is rising overall
   {
     if(reading > lastRead) //if we are still rising
     {
       lastRead = reading; //then carryonmywayardson.wav
     }
     else //if the new reading is lower
     {
       if(reading < (lastRead - 50)) //and it has dropped past the noise threshold 
       {
         peaks++; //then we passed a peak, increment counter
         low = true; //and the signal will now be dropping
         lastRead = 1023; //reset the last read
       }
     }
   }
  }
  
  freq = (1000000*peaks)/(lastPeak); //calculate the frequency of the signal by dividing the peaks by the number of seconds
  if( freq > 10000 && freq < 16000) //if the calculated frequency is within 3 kHz of what we want
  {
    idPositive = true; //then we did locate the signal
    //Serial.println();
    //Serial.print("freq");
    //Serial.println(freq);
  }

  
  
  sensor_L = (readingHigh - readingLow); //calculate the amplitude of the signal on the left sensor
  if(sensor_L < 150) //if the amplitude is lower than a minimum threshold
  {
    if(sensor_L < L_min) //and is lower than the low calibration point
    {
      L_min = sensor_L; //then this becomes the low calibration point
    }
    sensor_L = L_min; //our signal is below the threshold and will be set to the low calibration point, becoming effectively 0
  }
  if(sensor_L > L_max) //if the sensor is amazingly high
  {
     L_max = sensor_L; //then give it a prize
  }

////////////////////////////////////////////////////////////////////////////////////////////////


  peaks = 0;   //reset the peaks we have seen to 0
  lastRead = 0;//reset the last reading value
  reading = analogRead(centerSensorPin); //trigger a reading on the left sensor to start ADC switch process
  readingHigh = 0; //initialize at oposite end 
  readingLow = 1023; //see above
  //delay(10); //wait for ADC to finish switching
  lastPeak = micros(); //set time of reading start\
  
  //take 100 readings from the left sensor and store the values  
  for (unsigned int j = 0; j < 100; j++) 
  {  
    buffer[j] = analogRead(centerSensorPin);
  }
  
  lastPeak = micros() - lastPeak; //set time elapsed for readings
  
  //for each of the readings we took
  for (unsigned int j = 0; j < 100; j++) 
  {  
    reading = buffer[j]; //set a local variable to hold value
    
    if(reading < readingLow) //if the reading is lower than any we have seen this pass
    {
      readingLow = reading; //set the low point for the amplitude 
    } 
    else if(reading > readingHigh) //otherwise, if the reading is higher than any this pas
    {
     readingHigh = reading; //set he high point for the amplitude
    }
   
   if(low) //if the signal is dropping overall
   {
      if(reading < lastRead) //and the current reading is lower than the last reading
     {
       lastRead = reading; //then set this reading as the last reading and move on
     }
     else //otherwise
     { 
       if(reading > (lastRead + 50)) //if the signal is rising more than the noise threshold
       {
         low = false; //then the signal is no longer dropping
         lastRead = 0; //reset the last read
       }
     }
   }
   else //if the signal is rising overall
   {
     if(reading > lastRead) //if we are still rising
     {
       lastRead = reading; //then carryonmywayardson.wav
     }
     else //if the new reading is lower
     {
       if(reading < (lastRead - 50)) //and it has dropped past the noise threshold 
       {
         peaks++; //then we passed a peak, increment counter
         low = true; //and the signal will now be dropping
         lastRead = 1023; //reset the last read
       }
     }
   }
  }
  
  freq = (1000000*peaks)/(lastPeak); //calculate the frequency of the signal by dividing the peaks by the number of seconds
  if( freq > 10000 && freq < 16000) //if the calculated frequency is within 3 kHz of what we want
  {
    idPositive = true; //then we did locate the signal
    //Serial.println();
    //Serial.print("freq");
    //Serial.println(freq);
  }

  
  
  sensor_C = (readingHigh - readingLow); //calculate the amplitude of the signal on the left sensor
  if(sensor_C < 150) //if the amplitude is lower than a minimum threshold
  {
    if(sensor_C < C_min) //and is lower than the low calibration point
    {
      C_min = sensor_C; //then this becomes the low calibration point
    }
    sensor_C = C_min; //our signal is below the threshold and will be set to the low calibration point, becoming effectively 0
  }
  if(sensor_C > C_max) //if the sensor is amazingly high
  {
     C_max = sensor_C; //then give it a prize
  }



//The New Method////////////////////////////////////////////////////////////////////////////////

/*  This method takes the inputs from the three sensors and converts them into a reasonable input for the PID Controller.
 *  Instead of using the values directly to calculate a value, this new method breaks the inputs down into a number of unique cases
 *  that allow for the current state of the vehicle to be represented in 11 different positions relative to the line, effectively emulating the
 *  sensor arrays seen on standard pathfollowing robots. In theory this can be expanded to have an infinite number of cases, but is limited by 
 *  the resolution and spacing of the sensors to provide unique conditions. In this setup case 5 is dead center. The PID input values range from
 *  0 to 512 with the setpoint being 255.
 */
  max_avg = (L_max + R_max + C_max)/3;
  
  if(sensor_L == 0)
  {
    if(sensor_R == 0)
    {
      if(sensor_C == 0)
      {
        if(last_case < 5)
        {
          last_case = 0;
          Input = 512;
        }
        else if(last_case > 5)
        {
          last_case = 10;
          Input = 0;
        }
      }
    }
    else if(sensor_C == 0)
    {
      if(sensor_R < max_avg*0.6)
      {
        last_case = 9;
        Input = 20;
      }
      else
      {
        last_case = 8;
        Input = 35;
      }
    }
    else if(sensor_C < max_avg * 0.6)
    {
      last_case = 7;
      Input = 105;
    }
    else
    {
      if((sensor_C - sensor_R) > max_avg * 0.3)
      {
        last_case = 5;
        Input = 255;
      }
      else
      {
        last_case = 6;
        Input = 180;
      } 
    }
  }
  else if(sensor_R == 0)
  {
    if(sensor_C == 0)
    {
      if(sensor_L < max_avg*0.6)
      {
        last_case = 1;
        Input = 492;
      }
      else
      {
        last_case = 2;
        Input = 475;
      }
    }
    else if(sensor_C < max_avg*0.6)
    {
      last_case = 3;
      Input = 405;
    }
    else
    {
      if((sensor_C - sensor_L) > max_avg * 0.3)
      {
        last_case = 5;
        Input = 255;
      }
      else
      {
        last_case = 4;
        Input = 330;
      }
    }
  }
  else
  {
    if(sensor_C > max_avg*0.78)
    {
      last_case = 5;
      Input = 255;
    }
    else if((sensor_L - sensor_R) > max_avg * 0.6)
    {
      last_case = 2;
      Input = 475;
    }
    else if((sensor_R - sensor_L) > max_avg * 0.6)
    {
      last_case = 8;
      Input = 35;
    }
    else
    {
      Serial.println("Logic Error");
      Serial.print("  L:");
      Serial.print(sensor_L);
      Serial.print("  C:");
      Serial.print(sensor_C);
      Serial.print("  R:");
      Serial.println(sensor_R);
    }
  }
       pidControl.Compute();
       speedL = (Output+255)/4; //40+(int)(((float)(Output+255)/12)); 
       speedR = (-1*Output+255)/4; //40+(int)(((float)(-1*Output+255)/12));
//   Serial.write("L: ");
//   Serial.println(sensor_L);
//   Serial.write("R: ");
//   Serial.println(sensor_R);
//   Serial.println();     
//Serial.print("Case: ");
//Serial.print(last_case);
//Serial.print("  L:");
//Serial.print(sensor_L);
//Serial.print("  C:");
//Serial.print(sensor_C);
//Serial.print("  R:");
//Serial.println(sensor_R);
//////////////////////////////////////////////////////////////////////////////////

//    if(sensor_L > sensor_R)
//    {
//      if(sensor_R == 0)
//      {
//        Input = 512;
//      }
//      else
//      {
//        Input = ((float)((float)(sensor_L-L_min)/(L_max-L_min)) - ((float)(sensor_R-R_min)/(R_max-R_min)))*255 +255;
//      }
//      last_left = true;
//    }
//    else if( sensor_R > sensor_L)
//    {
//      if(sensor_L == 0)
//      {
//        Input = 0;
//      }
//      else
//      {
//        Input = ((float)((float)(sensor_L-L_min)/(L_max-L_min)) - ((float)(sensor_R-R_min)/(R_max-R_min)))*255 +255;
//      }
//      last_left = false;
//    }
//    else
//    {
//      if(last_left)
//      {
//        Input = 512;
//      }
//      else
//      {
//        Input = 0;
//      }
//    }
    

    // Serial.println(Input);
    //FREQUENCY AQUISITION//
    if(!idPositive) //if we cannot find the signal
    {
      if(on_path) //but the last time we checked, we were on the path
      {
        last_path_time = millis();
        on_path = false;
      }
      else
      {
        if( millis() > (last_path_time + 1000))
         {
           speedL = 0;
           speedR = 0;
         } 
       }
     }
    else //if we can find the signal
    {
      on_path = true;
      //calculate the motor outputs based on the PID output
      //speedL = (int)(obstacleScalar*((float)(Output+1023)/8)); 
     // speedR = (int)(obstacleScalar*((float)(-1*Output+1023)/8));
     
//     Serial.write("Input: ");
//     Serial.println(Input);
//     Serial.write("Output: ");
//     Serial.println(Output);
//     Serial.println();
     /*
     if(L_max > 400)
     {
       L_max = L_max - 1;
     }
     if(R_max > 400)
     {
       R_max = R_max - 1;
     }
     if(L_min < 300)
     {
       L_min = L_min + 1;
     }
     if(R_min < 300)
     {
       R_min = R_min + 1;
     }
     */
     //pidControl.Compute();
     //no obstacle sensor=
    }
    
//     Serial.write("L: ");
//     Serial.println(speedL);
//     Serial.write("R: ");
//     Serial.println(speedR);
//     Serial.println();

     speed_offset = 0.25*(abs(speedL - speedR));
     
//     speedL -= speed_offset;
//     speedR -= speed_offset;
     
     if(speedL < 0)
     {
       speedL = 0;
       //Serial.print("WHAT?!");
     }
     else if(speedL > maxSpeed)
     {
       speedL = maxSpeed;
     }
     if(speedR < 0)
     {
       speedR = 0;
       //Serial.println(" HOW?!");
     }
     else if(speedR > maxSpeed)
     {
       speedR = maxSpeed;
     }
     
  //sensorAverageL = (float)(alpha)*((float)(sensor_L-L_min)/(L_max-L_min)) + ((float)(1 - alpha) * sensorAverageL);
  //delay(10);
  //sensorAverageR = (float)(alpha)*((float)(sensor_R-R_min)/(R_max-R_min)) + ((float)(1 - alpha) * sensorAverageR);
  //delay(10);
  
 // speedL =(int)(obstacleScalar*255*(1-sensorAverageL));
  //speedR =(int)(obstacleScalar*255*(1-sensorAverageR));

  /*
   Serial.write('L');
   Serial.println(speedL);
   Serial.write('R');
   Serial.println(speedR);
   /*
  */
     //and write the outputs to the motors
     
        
//   Serial.write("L: ");
//   Serial.println(sensor_L);
//   Serial.write("R: ");
//   Serial.println(sensor_R);
//   Serial.println();

     
     analogWrite(motor_L, speedL);
     digitalWrite(pinI4, HIGH);//turn DC Motor B move clockwise
     digitalWrite(pinI3, LOW);

     analogWrite(motor_R, speedR);
     digitalWrite(pinI2,LOW);//turn DC Motor A move anticlockwise
     digitalWrite(pinI1,HIGH);
      
}

void loop()
{
  ///////////////
  //**RF LOOP**//
  ///////////////
  updateRF();
  
  /////////////////////
  //**OBSTACLE LOOP**//
  /////////////////////
  updateObstacle();
  
  /////////////////////
  //**GUIDANCE LOOP**//
  /////////////////////
  
  if(millis() >= (lastUpdate+update_rate))
  {
    updateMotors();
    lastUpdate = millis();
  }

}
